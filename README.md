## Getting Started

### Notes
This serverless application is hosted in AWS using AWS API Gateway, AWS Lambda, DynamoDB as database, AWS Cognito as user registration, authentication and authorization service, and ClaudiaJS as a deployment tool.

P.S - Since there's 6 options to choose from each questions and lowest possibly profile D is equal or greater than 2, I've modified the range score from 1 to 6 instead of 1 to 5. Sorry if I misunderstood the requirement.

### Development Prerequisites

AWS cli and ClaudiaJS are necessary to perform deployment to AWS.

[AWS CLI Installation](https://aws.amazon.com/cli/)

[ClaudiaJS Installation](https://claudiajs.com/tutorials/installing.html)

Please configure the AWS and ClaudiaJS before deployment.

#### Environment Variables
Please include a environment file **env.json** at root project
```
{
	"region": "REGION",
	"userPoolId": "COGNITO_USER_POOL_ID"
}
```

### Test

[AVA](https://github.com/avajs/ava) is used as a test framework to run separate test file parallelly.

To start the test
```
$ yarn test
```
Lint

[Standardjs](https://standardjs.com/) used for code linting

```
yarn lint
```

### Create DynamoDB
```
aws dynamodb create-table --table-name REPLACE_TABLE_NAME \
  --attribute-definitions AttributeName=REPLACE_ATTRIBUTE_NAME,AttributeType=S \
  --key-schema AttributeName=REPLACE_ATTRIBUTE_NAME,KeyType=HASH \
  --provisioned-throughput ReadCapacityUnits=1,WriteCapacityUnits=1 \
  --region us-east-1 \
  --query TableDescription.TableArn --output text
```

### Create Cognito User Pool
```
aws cognito-idp create-user-pool \           
--region "us-east-1" \
--pool-name "REPLACE_YOUR_USER_POOL_NAME" \
--policies "PasswordPolicy={MinimumLength=6,RequireUppercase=true,RequireLowercase=true,RequireNumbers=true,RequireSymbols=true}" \
--auto-verified-attributes "email" \
--alias-attributes "email"

```

### Deployment

First Deploy
```
$ yarn deploy
```

Changes Update
```
$ claudia update
```

### Development API Usage
Create User
```
curl -X POST \
  https://m1l3nzvft2.execute-api.us-east-1.amazonaws.com/dev/user/create \
  -d '{
	"username": "username",
	"email": "username@hello.me",
	"selectedInput": [
		{ "title": "loan", "amount": 0 },
		{ "title": "saving", "amount": 2000 }
	]
}'
```

List all User
```
curl -X GET \
  https://m1l3nzvft2.execute-api.us-east-1.amazonaws.com/dev/user/list
```

Create Profiling Question
```
curl -X POST \
  https://m1l3nzvft2.execute-api.us-east-1.amazonaws.com/dev/profiling-question/create \
  -d '{
	"title": "saving",
	"scores": [
		{ "amount": 0, "score": 1 },
	    { "amount": 2000, "score": 2 },
	    { "amount": 4000, "score": 3 },
	    { "amount": 6000, "score": 4 },
	    { "amount": 8000, "score": 5 },
	    { "amount": 10000, "score": 6 }
	  ]
}'
```

List all Profiling Question
```
curl -X GET \
  https://m1l3nzvft2.execute-api.us-east-1.amazonaws.com/dev/profiling-question/list
```

### Things to be improved
- CI / CD Pipeline Automation
- Intergration Test for each deploy
- Automation Script for creating AWS Resources and Services