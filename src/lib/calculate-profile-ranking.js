const calculateProfileRank = async (userInput, profilingQuestions, profileScore) => {
  try {
    const totalScore = await calculateTotalScore(userInput, profilingQuestions)
    const rank = calculateRank(totalScore, profileScore)

    return Promise.resolve(rank)
  } catch (err) {
    return Promise.reject(err)
  }
}

const calculateTotalScore = (userInput, profilingQuestions) => {
  const result = userInput.reduce((result, input) => {
    try {
      const question = profilingQuestions.find(q => q.title === input.title)
      if (!question) throw new Error('not found')

      const rank = question.scores.find(s => s.amount === input.amount) || {}
      if (!rank) throw new Error('not found')

      return {
        total: result.total + rank.score,
        errors: ''
      }
    } catch (err) {
      return { total: 0, errors: result.errors.concat(`${input.title}: ${input.amount}`) }
    }
  }, { total: 0, errors: [] })

  if (result.total === 0) {
    return Promise.reject(new Error(`${result.errors} not found in database`))
  } else {
    return Promise.resolve(result.total)
  }
}

const calculateRank = (totalScore, profileScore) => {
  // sort profile score in ascending order
  const sortedProfileScore = profileScore.sort((a, b) => {
    return a.score - b.score
  })

  let rank
  for (let i = 0; i < sortedProfileScore.length; i++) {
    // check if total score is larger or equal to current score
    if (sortedProfileScore[i].score <= totalScore) {
      rank = sortedProfileScore[i].rank
    }
  }

  return rank
}

export {
  calculateRank,
  calculateTotalScore,
  calculateProfileRank
}
