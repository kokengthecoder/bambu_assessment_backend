import test from 'ava'
import {
  calculateTotalScore,
  calculateProfileRank
} from '../calculate-profile-ranking'

// mock data
const profilingQuestions = [
  {
    title: 'loan',
    scores: [
      { amount: 0, score: 6 },
      { amount: 2000, score: 5 },
      { amount: 4000, score: 4 },
      { amount: 6000, score: 3 },
      { amount: 8000, score: 2 },
      { amount: 10000, score: 1 }
    ]

  }, {
    title: 'saving',
    scores: [
      { amount: 0, score: 1 },
      { amount: 2000, score: 2 },
      { amount: 4000, score: 3 },
      { amount: 6000, score: 4 },
      { amount: 8000, score: 5 },
      { amount: 10000, score: 6 }
    ]
  }]

const profileRank = [
  { rank: 'A', score: 8 },
  { rank: 'B', score: 6 },
  { rank: 'C', score: 4 },
  { rank: 'D', score: 2 }
]

/*
------------------
Test Start here
------------------
*/

// Test calculateTotalScore
test('Test function calculateTotalScore: should get 8', async t => {
  let userInput = [
    { title: 'loan', amount: 0 },
    { title: 'saving', amount: 2000 }
  ]

  t.is(await calculateTotalScore(userInput, profilingQuestions), 8)
})

test('Test function calculateTotalScore: should get error message', async t => {
  let userInput = [
    { title: 'loan', amount: 0 },
    { title: 'income', amount: 2000 }
  ]

  try {
    await calculateTotalScore(userInput, profilingQuestions)
  } catch (err) {
    t.is(err.message, 'income: 2000 not found in database')
  }
})

// Test calculateProfileRank
test('calculate A profile score', async t => {
  let userInput = [
    { title: 'loan', amount: 0 },
    { title: 'saving', amount: 2000 }
  ]

  t.is(await calculateProfileRank(userInput, profilingQuestions, profileRank), 'A')
})

test('calculate D profile score', async t => {
  let userInput = [
    { title: 'loan', amount: 10000 },
    { title: 'saving', amount: 2000 }
  ]

  t.is(await calculateProfileRank(userInput, profilingQuestions, profileRank), 'D')
})
