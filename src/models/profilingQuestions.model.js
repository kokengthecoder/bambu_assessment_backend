// import library
import AWS from 'aws-sdk'
import uuidv1 from 'uuid/v1'
import config from '../config'

// setup AWS DynamoDB
const dynamoDb = new AWS.DynamoDB.DocumentClient({
  region: config.region
})

const createProfilingQuestion = (body) => {
  var params = {
    TableName: 'profiling',
    Item: {
      uuid: uuidv1(),
      title: body.title,
      scores: body.scores
    }
  }

  return dynamoDb.put(params).promise()
}

const listProfilingQuestions = () => {
  return dynamoDb.scan({ TableName: 'profiling' }).promise()
    .then(response => response.Items)
}

export default {
  createProfilingQuestion,
  listProfilingQuestions
}
