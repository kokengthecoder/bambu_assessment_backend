// import library
import AWS from 'aws-sdk'
import config from '../config'

// setup AWS-IDP
const cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider({
  region: config.region
})

const createUser = (body) => {
  var params = {
    UserPoolId: config.userPoolId,
    Username: body.username,
    UserAttributes: [
      {
        Name: 'email', /* required */
        Value: body.email
      },
      {
        Name: 'profile',
        Value: body.profile
      }
    ]
  }

  return cognitoidentityserviceprovider.adminCreateUser(params).promise()
}

const listUsers = () => {
  var params = {
    UserPoolId: config.userPoolId /* required */
  }

  return cognitoidentityserviceprovider.listUsers(params).promise()
}

export default {
  createUser,
  listUsers
}
