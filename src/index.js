// import library
import ApiBuilder from 'claudia-api-builder'
import User from './models/users.model'
import ProfilingQuestions from './models/profilingQuestions.model'
import { calculateProfileRank } from './lib/calculate-profile-ranking'
import config from './config'

// configure library
let api = new ApiBuilder()

// create user api path
api.post('/user/create', async (request) => {
  try {
    const profilingQuestions = await ProfilingQuestions.listProfilingQuestions()
    const profile = await calculateProfileRank(
      request.body.selectedInput,
      profilingQuestions,
      config.rankingScore
    )
    const user = await User.createUser({ ...request.body, profile })
    return user
  } catch (err) {
    throw err
  }
}, { success: 201 })

// list all users api path
api.get('/user/list', async () => {
  try {
    return await User.listUsers()
  } catch (err) {
    throw err
  }
})

// create profiling question api path
api.post('/profiling-question/create', async (request) => {
  try {
    return await ProfilingQuestions.createProfilingQuestion(request.body)
  } catch (err) {
    throw err
  }
})

// list all profiling questions api path
api.get('/profiling-question/list', async () => {
  try {
    return await ProfilingQuestions.listProfilingQuestions()
  } catch (err) {
    throw err
  }
})

module.exports = api
