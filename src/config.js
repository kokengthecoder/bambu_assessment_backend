export default {
  userPoolId: process.env.userPoolId,
  region: process.env.region,
  rankingScore: [
    { rank: 'A', score: 8 },
    { rank: 'B', score: 6 },
    { rank: 'C', score: 4 },
    { rank: 'D', score: 2 }
  ]
}
